from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By 
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Utils import make_url
from Models import QuestionModel, save_questions_to_csv
from properties import properties
from cookies import cookies
import os
import time

from Bot import Bot

class Scraper:

    def __init__(self, bot : Bot) -> None:    
        self.bot = bot    
        self.driver = bot.driver
    
    def get_questions(self) -> None:
        finish = False
        self.driver.get(make_url('base', 'problems', '/4?page=8'))
        try:
            while not finish:
                questoes_ele = WebDriverWait(self.driver, 20).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'tbody > tr')))       
                questoes = []
                for questao in questoes_ele:
                    questao_model = self.get_question(questao)
                    if questao_model != None and questao_model.get_id():
                        questoes.append(questao_model)
                        time.sleep(3)
                    
                self.save_questions(questoes)
                # atualizando a arvore de arquivos
                self.bot.tree_callback()

                try:
                    finish = self.driver.find_element(by=By.CLASS_NAME, value='next.disabled')                
                    self.bot.logs_callback('Todas as questões foram capturadas...')
                    self.driver.quit()
                except NoSuchElementException:
                    # carregando proxima pagina
                    self.bot.logs_callback('Carregando próxima página...')
                    proxima_pagina = self.driver.find_element(by=By.CSS_SELECTOR, value='a[rel=next]')
                    self.driver.get(proxima_pagina.get_attribute('href'))

        except Exception as e:
            self.bot.logs_callback(f'Ocorreu um erro ao obter as questões: {e}')
        finally:
            self.driver.quit()
    
    def get_question(self, element : WebElement) -> QuestionModel | None:
        
        questao = QuestionModel()
        questao_id = None
        try:
            # lendo o id da questao
            questao_id = element.find_element(by=By.CSS_SELECTOR, value='td.id')
        except NoSuchElementException:
            return None
        
        # lendo o nome da questao
        questao_nome = element.find_element(by=By.CSS_SELECTOR, value='td.large')
        # lendo o nome da questao
        questao_solved = element.find_elements(by=By.CSS_SELECTOR, value='td.small')
        # lendo o nome da questao
        questao_nivel = element.find_elements(by=By.CSS_SELECTOR, value='td.tiny')

        questao_url_element = element.find_element(by=By.TAG_NAME, value='a')
        questao_url = '/'.join(questao_url_element.get_attribute('href').split('/')[5:])

        if questao_id: 
            questao.set_id(questao_id.text.rstrip())

        if questao_nome: 
            questao.set_title(questao_nome.text.rstrip())

        if questao_solved: 
            questao.set_solved(questao_solved[1].text.rstrip())

        if questao_nivel: 
            questao.set_nivel(questao_nivel[1].text.rstrip())
        
        # obtendo a descricao da questao em outra aba
        questao.set_description(self.get_question_description(questao_url))

        self.bot.logs_callback(f'Questão {questao.get_id()} | {questao.get_title()} obtida com sucesso...')

        return questao


    def get_question_description(self, question_url : str) -> str | None:        
        self.driver.switch_to.new_window('description_tab')        
        self.driver.get(make_url('base', f'/{question_url}'))
        # entrando no iframe
        self.driver.switch_to.frame('description-html')
        description = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, 'description')))         
        _description = None
        if description:
            _description = description.text
        time.sleep(1)
        self.driver.close()    
        self.back_to_main_window()
        return _description

    def save_questions(self, questions : list[QuestionModel]) -> None:
        category = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#page-name-c > h1')))
        category = '_'.join(category.text.lower().split(' '))

        page_element = self.driver.find_element(by=By.ID, value='table-info')
        page = page_element.text.split('OF')[0].strip()

        questions_path = properties['questions']
        if not os.path.exists(questions_path):
            os.mkdir(questions_path)
        
        category_path = f'{questions_path}/{category}'
        if not os.path.exists(category_path):
            os.mkdir(category_path)

        page_path = f'{category_path}/{page}'
        if not os.path.exists(page_path):
            os.mkdir(page_path)

        save_questions_to_csv(page_path, questions)
    
    def back_to_main_window(self):
        self.driver.switch_to.window(self.driver.window_handles[0])
        


        
        


