from selenium.webdriver import *

from .Login import Login
from .Scraper import Scraper
from properties import properties
from typing import Callable

from Gui import Gui


class Bot:

    def __init__(self, main : Gui) -> None:
        self.logs_callback  = main.add_log
        self.tree_callback  = main.ui_init_tree_visualizer

    def init(self) -> None:

        service = ChromeService(executable_path='web_scrapping/driver/chromedriver.exe')
        options = ChromeOptions()
        options.add_argument('--user-data-dir='+properties['data'])
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_experimental_option('useAutomationExtension', False)
        options.add_argument('--disable-blink-features=AutomationControlled')

        self.driver = Chrome(service=service, options=options)
    
    def login(self) -> None:
        login = Login(self)
        login.login()

    def scraper(self) -> None:
        scraper = Scraper(self)
        scraper.get_questions()        

    def is_closed(self) -> bool:
        try:
            _ = self.driver.title
            return False
        except Exception as e:
            return True