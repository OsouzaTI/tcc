from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By 
from properties import properties
from Utils import send_keys_delay, make_url
from Bot import Bot
class Login:

    def __init__(self, bot : Bot) -> None:
        self.bot = bot
        self.driver = bot.driver

    def login(self) -> None:
        self.driver.get(make_url('base', 'login'))
        try:
            
            # buscando campo de email
            email = self.driver.find_element(by=By.ID, value='email')        
            email.send_keys(properties['email'])
            # buscando campo de senha
            senha = self.driver.find_element(by=By.ID, value='password')
            senha.send_keys(properties['senha'])

            # lembre-me
            lembre = self.driver.find_element(by=By.ID, value='remember-me')
            lembre.click()

            # clicando em login
            logar = self.driver.find_element(by=By.CSS_SELECTOR, value='input.send-purple.send-right')
            logar.click()

        except Exception as e:
            self.bot.logs_callback(f'Não foi possível logar na pagina...')

