from selenium.webdriver.remote.webelement import WebElement
import time

def send_keys_delay(element : WebElement, word : str, delay : float) -> None:
    element.click()
    for key in word:
        element.send_keys(key)
        time.sleep(1)