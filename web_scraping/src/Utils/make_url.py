from properties import properties

def make_url(*keys) -> str:
    url = ''
    for key in keys:
        try:
            url += properties['urls'][key]
        except Exception as e:
            url += key
    return url
