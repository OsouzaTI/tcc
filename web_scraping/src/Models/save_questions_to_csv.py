from .QuestionModel import QuestionModel
import pandas as pd
import os
from properties import properties

def save_questions_to_csv(path : str, questions : list[QuestionModel]) -> None:
    # Nomes das colunas
    column_names = ["ID", "TITLE", "SOLVED", "NIVEL", "DESCRIPTION"]
    ls = [ question.get_attributes() for question in questions ]
    df = pd.DataFrame(ls, columns=column_names)
    df.to_csv(f'{path}/questions.csv', index=False)

def combine_all_questions_to_csv(path : str) -> None:
    base_path   = properties['questions']
    final_path  = f'{base_path}/{path}'
    # Lista para armazenar os DataFrames de cada arquivo questions.csv
    dataframes = []

    # Percorre as pastas e lê os arquivos questions.csv
    for folder_name in os.listdir(final_path):
        folder_path = os.path.join(final_path, folder_name)
        csv_path = os.path.join(folder_path, "questions.csv")        
        if os.path.isfile(csv_path):            
            df = pd.read_csv(csv_path)
            dataframes.append(df)
    # Concatena todos os dataframes em um único dataframe
    combined_df = pd.concat(dataframes, ignore_index=True)
    combined_df.to_csv(f'{final_path}/all_questions.csv')