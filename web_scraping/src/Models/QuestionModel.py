class QuestionModel:

    def __init__(self) -> None:
        self.id = None
        self.title = None
        self.solved = None
        self.nivel = None
        self.description = None

    def get_id(self):
        return self.id

    def set_id(self, value):
        self.id = value

    def get_title(self):
        return self.title

    def set_title(self, value):
        self.title = value

    def get_solved(self):
        return self.solved

    def set_solved(self, value):
        self.solved = value

    def get_nivel(self):
        return self.nivel

    def set_nivel(self, value):
        self.nivel = value

    def get_description(self):
        return self.description

    def set_description(self, value):
        self.description = value
    
    def get_attributes(self) -> list:
        return [self.id, self.title, self.solved, self.nivel, self.description]
