from PyQt5 import QtWidgets, QtGui, uic
from PyQt5.QtWidgets import QFileSystemModel, QTreeWidgetItem
from PyQt5.QtCore import QStringListModel, QAbstractTableModel, QModelIndex, Qt

from threading import Thread

from Bot import Bot
from properties import properties

import os
import time
import pandas as pd

from Models import combine_all_questions_to_csv

class CSVTableModel(QAbstractTableModel):
    def __init__(self, data):
        super().__init__()
        self.data = data

    def rowCount(self, parent=None):
        return len(self.data)

    def columnCount(self, parent=None):
        return len(self.data.columns)

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            return str(self.data.iloc[index.row(), index.column()])

class Gui(QtWidgets.QMainWindow, Thread):
    
    def __init__(self, title : str = "Questions Extractor") -> None:
        super().__init__()
        self.logs = []

        # carregando janela
        uic.loadUi('gui/main.ui', self)
        self.setWindowTitle(title)

        # inicializando interface
        self.ui_init_tree_visualizer()
        self.ui_init_list_logs()
        self.ui_init_table_csv()

        self.tree_visualizer.setContextMenuPolicy(Qt.CustomContextMenu)  
        self.tree_visualizer.customContextMenuRequested.connect(self.ui_tree_context_menu_event) 

        # Inicializando o bot
        self.bot = Bot(self)
        self.bot.init()

    def run(self) -> None:
        
        while not self.bot.is_closed():
            pass
            # self.bot.login()
            # self.bot.scraper()
        
        print('Thread finalizada...')

    def ui_init_table_csv(self) -> None:
        self.tb_data : QtWidgets.QTableView = self.findChild(QtWidgets.QTableView, 'tb_data')

    def set_table_csv(self, path : str) -> None:
        data = pd.read_csv(path)
        model = CSVTableModel(data)
        self.tb_data.setModel(model)

    def ui_init_list_logs(self) -> None:
        self.lt_logs : QtWidgets.QListView = self.findChild(QtWidgets.QListView, 'lt_logs')
        self.log_model = QStringListModel()
        self.lt_logs.setModel(self.log_model)
        self.refresh_logs()


    def add_log(self, log : str) -> None:        
        current_time = time.strftime("%H:%M:%S")
        log = f"{current_time}: {log}"
        self.logs.append(log)
        self.refresh_logs()
    
    def refresh_logs(self) -> None:
        self.log_model.setStringList(self.logs)

    def ui_init_tree_visualizer(self) -> None:

        self.tree_visualizer : QtWidgets.QTreeWidget = self.findChild(QtWidgets.QTreeWidget, 'tree_visualizer')
        self.tree_visualizer.clear()
        self.tree_visualizer.setHeaderLabels(["Name", "Size"])
        self.tree_visualizer.doubleClicked.connect(self.tree_item_double_clicked)

        root_path = properties['questions']
        root_item = QTreeWidgetItem(self.tree_visualizer, [os.path.basename(root_path), ""])
        self.tree_visualizer.addTopLevelItem(root_item)
        self.populate_tree(root_path, root_item)


    def populate_tree(self, path, parent_item):
        for entry in os.listdir(path):
            full_path = os.path.join(path, entry)
            size_kb = str(os.path.getsize(full_path) / 1024)[:4] + ' kb'
            item = QTreeWidgetItem(parent_item, [entry, size_kb])
            parent_item.addChild(item)
            if 'csv' in full_path:
                parent_item.setIcon(0, QtGui.QIcon('gui/icons/file.png'))
            else:
                parent_item.setIcon(0, QtGui.QIcon('gui/icons/folder.png'))

            if os.path.isdir(full_path):
                self.populate_tree(full_path, item)

    def tree_item_double_clicked(self, item : QTreeWidgetItem):
        texts = []
        item_data = item.data(0)
        while item_data is not None:
            texts.append(item_data)
            item = item.parent()
            item_data = item.data(0)
        path = "/".join(texts[::-1])
        if os.path.isfile(path):
            self.set_table_csv(path)

    def ui_tree_context_menu_event(self, position):
        self.menu_context = QtWidgets.QMenu(self.tree_visualizer)
        menu = QtWidgets.QMenu(self)

        combine_all_files = QtWidgets.QAction("Combine All Files", self)

        menu.addAction(combine_all_files)
        action = menu.exec_(self.tree_visualizer.mapToGlobal(position))

        index = self.tree_visualizer.indexAt(position)
        if action == combine_all_files:
            path = index.data(0)
            self.add_log(f'Combinando arquivos da pasta {path}...')
            combine_all_questions_to_csv(path)
            self.ui_init_tree_visualizer()
           

