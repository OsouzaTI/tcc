from PyQt5 import QtWidgets

from Gui import Gui

app = QtWidgets.QApplication([])
gui = Gui()
gui.show()
gui.start()
app.exec_()